const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const DB_URL = process.env.DB_URL || 'mongodb://localhost:27017/Node-Final-Project';

const connect = async() => {
    try {
        const db = await mongoose.connect(DB_URL, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
        });
        const { name, host } = db.connection;
        console.log(`Conectando con éxito a ${name} en ${host}`);
    } catch (err) {
        console.log(`Ha ocurrido un error conectando con la base de datos: ${err.message}`)
    }
};

module.exports = { DB_URL, connect };