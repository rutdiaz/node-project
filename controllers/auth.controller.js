const passport = require('passport');
const { setStrategies } = require('../auth/index')

const registerGet = (req, res) => {
    return res.render('./auth/register');
};

const registerPost = async(req, res, next) => {
    const done = (err, user) => {
        if (err) {
            return next(err);
        };

        req.login(user, (err) => {
            if (err) {
                return next(err);
            };

            return res.redirect('/articles');
        });

    }

    passport.authenticate("register-strategy", done)(req);

};

const loginGet = (req, res, next) => {
    return res.render("./auth/login");
};

const loginPost = (req, res, next) => {
    const done = (err, user) => {
        if (err) {
            return next(err);
        };

        req.login(user, (err) => {
            if (err) {
                return next(err);
            }

            return res.redirect('/articles')
        });
    };

    passport.authenticate("login-strategy", done)(req);

};

const logoutPost = (req, res, next) => {
    if (req.user) {
        req.logout();

        req.session.destroy(() => {
            res.clearCookie("connect.sid");
            return res.redirect('/');
        });
    }
};

module.exports = {
    registerGet,
    registerPost,
    loginGet,
    loginPost,
    logoutPost,
}