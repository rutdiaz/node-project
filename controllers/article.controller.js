const express = require('express');
const Article = require('../models/Article.model');
const { isAuth, isAdmin } = require('../middlewares/auth.middleware');
const { upload, uploadToCloudinary } = require('../middlewares/file.middleware');


const articlesGet = async(req, res, next) => {
    try {
        const { isAuth } = req;
        const articles = await Article.find();

        return res.render('./articles/articles', { articles, title: 'Nuestros vehículos', isAuth });
    } catch (err) {
        return next(err);
    }
};

const articleDetailGet = async(req, res, next) => {
    const { id } = req.params;
    const { isAuth } = req;

    try {
        const article = await Article.findById(id);
        return res.status(200).render("./articles/article", { article, isAuth });
    } catch (err) {
        return next(err);
    }
};

const articleCreateGet = (req, res, next) => {
    return res.render('./articles/create-article');
};

const articleCreatePost = async(req, res, next) => {
    try {
        const { model, type, year, brand, fuel, kilometres, color, price } = req.body;

        const newArticle = new Article({
            model,
            type,
            year,
            brand,
            fuel,
            kilometres,
            color,
            price,
            image: req.fileUrl ? req.fileUrl : '',
        });

        const createdArticle = await newArticle.save();
        console.log(createdArticle);

        return res.render('./articles/article', { article: createdArticle });
    } catch (err) {
        return next(err);
    }
};





module.exports = {
    articlesGet,
    articleDetailGet,
    articleCreateGet,
    articleCreatePost
}