const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../models/User.model');

const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const validatePassword = (password) => {
    const re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    return re.test(String(password));
};

const registerStrategy = new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
    async(req, email, password, done) => {
        try {
            //Compruebo que el usuario no existe.
            const existingUser = await User.findOne({ email });

            if (existingUser) {
                const err = new Error('Este usuario ya está registrado');
                return done(err);
            };

            //Validar el email introducido.
            const validEmail = validateEmail(email);

            if (!validEmail) {
                const err = new Error('Email invalido');
                return done(err);
            };

            //Validar la contraseña introducida.
            const validPassword = validatePassword(password);

            if (!validPassword) {
                const err = new Error('La constraseña debe contener de 6 a 20 carácteres y mínimo una letra mintúscula, una letra mayúscula y un numero');
                return done(err);
            };

            //Cuando esta todo validado, encriptamos la contraseña.
            const saltRounds = 10;
            const hash = await bcrypt.hash(password, saltRounds);

            //Crear la instancia del modelo User con los datos del usuario.
            const newUser = new User({
                email,
                password: hash,
                name: req.body.name,
            });

            //Guardar usuario y devoler el usuario creado
            const savedUser = await newUser.save();

            savedUser.password = null;
            return done(null, savedUser);

        } catch (err) {
            return done(err);
        }
    }

);

module.exports = registerStrategy;