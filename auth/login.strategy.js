const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User.model');
const bcrypt = require('bcrypt');

const loginStrategy = new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
    async(req, email, password, done) => {
        try {
            //Compruebo que el usuario existe.
            const existingUser = await User.findOne({ email });

            if (!existingUser) {
                const err = new Error('Este usuario no existe');
                err.status = 401;
                return done(err);
            };

            //Compruebo que la contraseña coincide con la del usuario.
            const validPassword = await bcrypt.compare(password, existingUser.password);

            if (!validPassword) {
                const err = new Error('La contraseña no es correcta');
                return done(err);
            };

            existingUser.password = null;
            return done(null, existingUser);

        } catch (err) {
            return done(err);
        }
    }
);

module.exports = loginStrategy;