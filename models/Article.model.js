const mongoose = require('mongoose');

const { Schema } = mongoose;

const articleSchema = new Schema({
    model: { type: String, required: true },
    type: { type: String, required: true, enum: ['coche', 'moto', 'furgoneta'], default: 'coche' },
    year: { type: Number, required: true },
    brand: { type: String, required: true },
    fuel: { type: String, required: true },
    kilometres: { type: String, required: true },
    color: { type: String },
    price: { type: String },
    image: { type: String }
}, { timestamps: true });

const Article = mongoose.model('Articles', articleSchema);

module.exports = Article;