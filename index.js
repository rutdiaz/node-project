const express = require('express');

const dotenv = require('dotenv');
dotenv.config();

const path = require('path');
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo');

const auth = require('./auth');
auth.setStrategies();

const methodOverride = require('method-override');

const authRoutes = require('./routes/Auth.routes');
const indexRoutes = require('./routes/index.routes');
const articlesRoutes = require('./routes/Articles.routes');

//const hbs = require('./config/hbs.config')
//hbs.createHelpers();
const db = require('./config/db.config.js');

db.connect();

const PORT = process.env.PORT || 3000;

const app = express();


app.use(session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 24 * 60 * 60 * 1000
    },
    store: MongoStore.create({ mongoUrl: db.DB_URL }),
}));

app.use(passport.initialize());

app.use(passport.session());

app.use((req, res, next) => {
    req.isAuth = req.isAuthenticated();
    next();
});

app.use(methodOverride('_method'));

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'views')));

app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'hbs');


app.use('/', indexRoutes);
app.use('/auth', authRoutes);
app.use('/articles', articlesRoutes);


app.use('*', (req, res, next) => {
    const err = new Error('Ruta no encontrada');

    return res.status(404).render('./index/error', {
        message: err.message,
        status: 404,
    });
});

app.use((error, req, res, next) => {

    console.log(error)
    return res.status(error.status || 500).render('./index/error', {
        message: error.message || 'Error inesperado',
        status: error.status || 500,
    });
});


app.listen(PORT, () => {
    console.log(`Servidor corriendo en http://localhost:${PORT}`);
})