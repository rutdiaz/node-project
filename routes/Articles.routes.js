const express = require('express');
const controller = require('../controllers/article.controller');
const { isAuth, isAdmin } = require('../middlewares/auth.middleware');
const { upload, uploadToCloudinary } = require('../middlewares/file.middleware');

const router = express.Router();

router.get('/', controller.articlesGet);
router.get('/create', [isAuth], controller.articleCreateGet);
router.post('/create', [isAuth, upload.single('image'), uploadToCloudinary], controller.articleCreatePost);
router.get('/:id', controller.articleDetailGet);



module.exports = router;