const express = require('express');
const { isAuth } = require('../middlewares/auth.middleware');

const router = express.Router();

router.get("/", (req, res, next) => {
    const msg = 'Bienvenidos al proyecto De Nuevo Sobre Ruedas';

    return res.render('./index/index', { title: 'Node Final Projct', msg, isAuthenticated: req.isAuthenticated(), user: req.user });
})

module.exports = router;