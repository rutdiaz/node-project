const isAuth = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next()
    }

    return res.status(401).render("index", "/auth/login");
}

const isAdmin = (req, res, next) => {
    if (req.isAuthenticated()) {
        if (req.user.role === "admin") {
            return next();
        }
        const err = new Error("Espacio reservado para administradores");
        err.status = 403;
        return next(err);
    }

    return res.redirect("/auth/login");
};

module.exports = {
    isAuth,
    isAdmin,
}