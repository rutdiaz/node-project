const multer = require('multer');
const fs = require('fs');
const path = require('path');
const cloudinary = require('cloudinary').v2;

const ACCEPTED_FILES_EXTENSIONS = ['image/png', 'image/jpg', 'image/jpeg'];

const storage = multer.diskStorage({
    filename: (req, file, callback) => {
        const fileName = `${Date.now()}-${file.originalname}`;
        callback(null, fileName);
    },
    destination: (req, file, callback) => {
        const directory = path.join(__dirname, '../public/uploads');
        callback(null, directory);
    },
});

const fileFilter = (req, file, cb) => {
    if (!ACCEPTED_FILES_EXTENSIONS.includes(file.mimetype)) {
        const err = new Error('Archivo inválido');
        err.status = 400;

        return cb(err, true);
    }
    return cb(null, true);
};

const upload = multer({
    storage,
    fileFilter,
});

const uploadToCloudinary = async(req, res, next) => {
    if (req.file) {
        const path = req.file.path;

        const avatar = await cloudinary.uploader.upload(path);

        req.fileUrl = avatar.secure_url;

        await fs.unlinkSync(path);

        return next();
    } else {
        return next();
    }
};

module.exports = {
    upload,
    uploadToCloudinary
}