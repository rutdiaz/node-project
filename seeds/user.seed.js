const mongoose = require('mongoose');
const User = require('../models/User.model');
const db = require('../config/db.config');

const users = [{
        email: 'ruteta@upgrade.com',
        pasword: 'ASDasd1234',
        name: 'Rut Elisenda Diaz Albert',
        role: 'admin'
    },
    {
        email: 'rutdiaz@upgrade.com',
        pasword: 'asdASD1234',
        name: 'Rut Diaz',
        role: 'user'
    }
];

mongoose
    .connect(db.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(async(dbConection) => {
        const { name, host } = dbConection.connection;
        console.log(`El seed se guardará en la base de datos ${name}, en ${host}`);

        const users = await User.find();

        if (users.length) {
            await User.collection.drop();
            console.log('Coleccion eliminada');
        }

    })
    .catch(err => console.log('Error eliminando la colección User', err))
    .then(async() => {
        await User.insertMany(users);
        console.log('Usuario añadido a la base de datos');

    })
    .catch(err => console.log('Error añadiendo usuario a la base de datos', err))
    .finally(() => mongoose.disconnect());