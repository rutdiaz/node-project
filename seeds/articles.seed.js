const mongoose = require('mongoose');
const Article = require('../models/Article.model');
const db = require('../config/db.config');

const articles = [{
        model: 'Peugeot 106',
        type: 'coche',
        year: 1998,
        brand: 'Peugeot',
        fuel: 'diesel',
        kilometres: '200.000',
        color: 'blanco',
        price: '1.500€'
    },
    {
        model: 'Mercedes-Benz Clase A',
        type: 'coche',
        year: 2015,
        brand: 'Mercedes-Benz',
        fuel: 'diesel',
        kilometres: '198.000',
        color: 'gris',
        price: '13.000€'
    },
    {
        model: 'AUDI A1',
        type: 'coche',
        year: 2017,
        brand: 'Audi',
        fuel: 'gasolina',
        kilometres: '16.544',
        color: 'blanco',
        price: '16.899€'
    },
    {
        model: 'Opel Astra',
        type: 'coche',
        year: 2019,
        brand: 'Opel',
        fuel: 'gasolina',
        kilometres: '1.000',
        color: 'azul',
        price: '17.000€'
    },
    {
        model: 'Dacia Dokker Essential',
        type: 'furgoneta',
        year: 2019,
        brand: 'Dacia',
        fuel: 'diesel',
        kilometres: '65.562',
        color: 'negro',
        price: '12.100€'
    },
    {
        model: 'Mercedes-Benz Viano',
        type: 'furgoneta',
        year: 2006,
        brand: 'Mercedes-Benz',
        fuel: 'diesel',
        kilometres: '290.000',
        color: 'gris',
        price: '1.500€'
    },
    {
        model: 'Opel Zafira',
        type: 'coche',
        year: 2018,
        brand: 'Opel',
        fuel: 'diesel',
        kilometres: '70.000',
        color: 'beige',
        price: '15.000€'
    },
    {
        model: 'Yamaha X-MAX',
        type: 'moto',
        year: 2018,
        brand: 'Yamaha',
        fuel: 'gasolina',
        kilometres: '7.850',
        color: 'rojo',
        price: '4.100€'
    },
    {
        model: 'Yamaha XJ 600',
        type: 'moto',
        year: 1999,
        brand: 'Yamaha',
        fuel: 'gasolina',
        kilometres: '37.000',
        color: 'verde',
        price: '1.100€'
    },
    {
        model: 'Peugeot 3008 Active',
        type: 'coche',
        year: 2012,
        brand: 'Peugeot',
        fuel: 'diesel',
        kilometres: '130.000',
        color: 'blanco',
        price: '7.000€'
    }
];

mongoose
    .connect(db.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(async(dbConection) => {
        const { name, host } = dbConection.connection;
        console.log(`El seed se guardará en la base de datos ${name}, en ${host}`);

        const articles = await Article.find();

        if (articles.length) {
            await Article.collection.drop();
            console.log('Colección Article eliminada correctamente')
        }

    })
    .catch(err => console.log('Error eliminando la colección Article', err))
    .then(async() => {
        await Article.insertMany(articles);
        console.log('Articulos añadidos a la base de datos');

    })
    .catch(err => console.log('Error añadiendo articulos a la base de datos', err))
    .finally(() => mongoose.disconnect());